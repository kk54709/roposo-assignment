//
//  StoriesViewController.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import UIKit
import SafariServices

class StoriesViewController: UIViewController, NavBarDelegate {
    
    var storyTableView: UITableView!
    var navView: NavigationBarView!
    
    var storyArray = [Story]()
    var userArray = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureSubviews()
        
        (self.storyArray , self.userArray) = JsonManager.sharedInstance.parseJsonData()
        storyTableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
  
    }
    
    // MARK : - Delegate Methods
    
    func backButtonTapped() {
        self.navigationController?.popViewControllerAnimated(true)
    }
}

extension StoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storyArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var storyCell: StoryCell? = tableView.dequeueReusableCellWithIdentifier("StoryCell", forIndexPath: indexPath) as? StoryCell
        
        if storyCell == nil {
            storyCell = StoryCell(style: UITableViewCellStyle.Default, reuseIdentifier: "StoryCell")
        }
        
        storyCell?.story = storyArray[indexPath.row]
        return storyCell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyDetailVC = StoryDetailsViewController()
        var selectedUser: User!
        
        for (index, item) in userArray.enumerate() {
            if (item.userID == storyArray[indexPath.row].db) {
                selectedUser = userArray[index]
                break
            }
        }
        
        print(selectedUser)
        storyDetailVC.story = storyArray[indexPath.row]
        storyDetailVC.user = selectedUser
        
        self.navigationController?.pushViewController(storyDetailVC, animated: true)
        
        //        let svc = SFSafariViewController(URL: NSURL(string: storyArray[indexPath.row].storyURL)!)
        //        self.presentViewController(svc, animated: true, completion: nil)
    }
    
}

extension StoriesViewController {
    
    // MARK: - UIView Setup
    
    func configureSubviews() {
        
        // Navigation Bar View
        
        navView = NavigationBarView()
        navView.delegate = self
        navView.titleText = "List of Stories"
        navView.backgroundColor = UIColor(red: 21/255, green: 107/255, blue: 255/255, alpha: 1)
        navView.backButton.hidden = true
        navView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = navView.topAnchor.constraintEqualToAnchor(self.view.topAnchor)
        let leadingConstraint = navView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
        let trailingConstraint = navView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
        let height = navView.heightAnchor.constraintEqualToConstant(64)
        
        self.view.addSubview(navView)
        
        // Story Table View
        
        storyTableView = UITableView()
        storyTableView.delegate = self
        storyTableView.dataSource = self
        storyTableView.rowHeight = UITableViewAutomaticDimension
        storyTableView.estimatedRowHeight = 250
        storyTableView.registerClass(StoryCell.self as AnyClass, forCellReuseIdentifier: "StoryCell")
        storyTableView.translatesAutoresizingMaskIntoConstraints = false
        storyTableView.separatorStyle = .None
        
        storyTableView.backgroundColor = UIColor(white: 0.8, alpha: 1)
        let storyTableTopConstraint = storyTableView.topAnchor.constraintEqualToAnchor(self.navView.bottomAnchor)
        let storyTableLeadingConstraint = storyTableView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
        let storyTableTrailingConstraint = storyTableView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
        let storyTableBottomConstraint = storyTableView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor)
        self.view.addSubview(storyTableView)
        
        NSLayoutConstraint.activateConstraints([topConstraint,leadingConstraint,trailingConstraint,height, storyTableTopConstraint, storyTableBottomConstraint, storyTableLeadingConstraint, storyTableTrailingConstraint])
    }

}
