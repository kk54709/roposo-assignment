//
//  Story.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import Foundation

struct Story {
    
    let storyID: String!
    let db: String!
    let title: String!
    let type: String!
    let titleDescription: String!
    let bgImageURL:  String!
    let storyURL: String!
    let isLiked: Bool!
    let likesCount: Int!
    let commentCount: Int!
}