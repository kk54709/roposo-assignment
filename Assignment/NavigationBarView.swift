//
//  NavigationBarView.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import UIKit

protocol NavBarDelegate: class{
    func backButtonTapped()
}

class NavigationBarView: UIView {
    
    var titleText: String? {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    var titleLabel: UILabel!
    var backButton: UIButton!
    weak var delegate: NavBarDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // extra function go in AFTER super.init
        configureSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
//        fatalError("init(coder:) has not been implemented")
    }

    func configureSubviews() {
        titleLabel = UILabel()
        titleLabel.font = UIFont(name: "System", size: 20)
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(titleLabel)
        
        let bottomConstraint = titleLabel.bottomAnchor.constraintEqualToAnchor(self.bottomAnchor,constant: -15)
        let xAxisConstraint = titleLabel.centerXAnchor.constraintEqualToAnchor(self.centerXAnchor)
        
        
        backButton = UIButton()
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.setImage(UIImage(named: "back"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(NavigationBarView.buttonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        
        let buttonBottomConstraint = backButton.bottomAnchor.constraintEqualToAnchor(self.bottomAnchor)
        let buttonLeadingConstraint = backButton.leadingAnchor.constraintEqualToAnchor(self.leadingAnchor)
        let buttonHeight = backButton.heightAnchor.constraintEqualToConstant(50)
        let buttonWidth = backButton.widthAnchor.constraintEqualToConstant(50)
        self.addSubview(backButton)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint,xAxisConstraint, buttonBottomConstraint, buttonLeadingConstraint, buttonHeight, buttonWidth])
    }
    
    func buttonTapped() {
        delegate?.backButtonTapped()
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
