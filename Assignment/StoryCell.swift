//
//  StoryCell.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import UIKit

class StoryCell: UITableViewCell {
    
    var backgroundImageView: UIImageView!
    var titleLabel: UILabel!
    var subTitleLabel: UILabel!
    
    var story: Story? {
        didSet {
            refreshCellSubViews()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupCellSubViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - UIView Setup
    
    func setupCellSubViews() {
        
        // Background UIView
        
        self.selectionStyle = .None
        self.contentView.backgroundColor = UIColor(white: 0.8, alpha: 1)
        
        let bgView = UIView()
        
        bgView.layer.cornerRadius = 4
        bgView.layer.shadowOffset = CGSize(width: 2, height: 2)
        bgView.layer.shadowRadius = 2
        bgView.layer.shadowColor = UIColor(white: 0.5, alpha: 1).CGColor
        bgView.layer.shadowOpacity = 1
        self.contentView.clipsToBounds = true
        self.contentView.layer.masksToBounds = true
        bgView.backgroundColor = UIColor.whiteColor()
        bgView.translatesAutoresizingMaskIntoConstraints = false
        let bgViewTopConstraint = bgView.topAnchor.constraintEqualToAnchor(self.contentView.topAnchor, constant: 8)
        let bgViewBottomConstraint = bgView.bottomAnchor.constraintEqualToAnchor(self.contentView.bottomAnchor, constant: 0)
        let bgViewleadingConstraint = bgView.leadingAnchor.constraintEqualToAnchor(self.contentView.leadingAnchor, constant: 8)
        let bgViewTrailingConstraint = bgView.trailingAnchor.constraintEqualToAnchor(self.contentView.trailingAnchor, constant: -8)
        
        self.contentView.addSubview(bgView)
        NSLayoutConstraint.activateConstraints([bgViewTopConstraint, bgViewBottomConstraint, bgViewleadingConstraint, bgViewTrailingConstraint])
        
        // Image View
        
        backgroundImageView = UIImageView()
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.clipsToBounds = true
        backgroundImageView.backgroundColor = UIColor.clearColor()
        backgroundImageView.contentMode = .Top
        let bgImageViewTopConstraint = backgroundImageView.topAnchor.constraintEqualToAnchor(bgView.topAnchor)
        let bgImageViewLeadingConstraint = backgroundImageView.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor)
        let bgImageViewTrailingConstraint = backgroundImageView.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor)
        let bgImageViewHeightConstraint = backgroundImageView.heightAnchor.constraintEqualToConstant(150)
        
        bgView.addSubview(backgroundImageView)
        NSLayoutConstraint.activateConstraints([bgImageViewTopConstraint, bgImageViewLeadingConstraint, bgImageViewTrailingConstraint, bgImageViewHeightConstraint])
        
        
        // Seperator View
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor.grayColor()
        seperatorView.translatesAutoresizingMaskIntoConstraints = false

        let seperatorViewTopConstraint = seperatorView.topAnchor.constraintEqualToAnchor(backgroundImageView.bottomAnchor)
        let seperatorViewLeadingConstraint = seperatorView.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor)
        let seperatorViewTrailingConstraint = seperatorView.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor)
        let seperatorViewHeightConstraint = seperatorView.heightAnchor.constraintEqualToConstant(1)
        
        bgView.addSubview(seperatorView)
        NSLayoutConstraint.activateConstraints([seperatorViewTopConstraint,seperatorViewLeadingConstraint,seperatorViewTrailingConstraint,seperatorViewHeightConstraint])
        
        // Title Label
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont(name: "Helvetica", size: 14)
        titleLabel.textColor = UIColor(white: 0.2, alpha: 1)
        titleLabel.text = ""
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let titleLabelTopConstraint = titleLabel.topAnchor.constraintEqualToAnchor(seperatorView.bottomAnchor, constant: 5)
        let titleLabelleadingConstraint = titleLabel.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor,constant: 5)
        let titleLabelTrailingConstraint = titleLabel.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor, constant: -5)
        
        bgView.addSubview(titleLabel)
        NSLayoutConstraint.activateConstraints([titleLabelTopConstraint, titleLabelTrailingConstraint, titleLabelleadingConstraint])
        
        // Description Label
        
        subTitleLabel = UILabel()
        subTitleLabel.numberOfLines = 0
        subTitleLabel.font = UIFont(name: "Helvetica", size: 12)
        subTitleLabel.text = ""
        subTitleLabel.textColor = UIColor(white: 0.4, alpha: 1)
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        let subTitleLabelTopConstraint = subTitleLabel.topAnchor.constraintEqualToAnchor(titleLabel.bottomAnchor, constant: 5)
        let subTitleLabelleadingConstraint = subTitleLabel.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor,constant: 5)
        let subTitleLabelTrailingConstraint = subTitleLabel.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor, constant: -5)
        let subTitleLabelBottomConstraint = subTitleLabel.bottomAnchor.constraintEqualToAnchor(bgView.bottomAnchor, constant: -5)
        
        bgView.addSubview(subTitleLabel)
        NSLayoutConstraint.activateConstraints([subTitleLabelTopConstraint, subTitleLabelleadingConstraint, subTitleLabelTrailingConstraint, subTitleLabelBottomConstraint])
    }
    
    // MARK: - Custom Methods
    
    func refreshCellSubViews() {
        if let imageURL = story?.bgImageURL {
            let url = NSURL(string: imageURL)
            backgroundImageView.sd_setImageWithURL(url, completed: { (image, error, cache, url) in
                
            })
            titleLabel.text = story?.title
            subTitleLabel.text = story?.titleDescription
        }
    }

}
