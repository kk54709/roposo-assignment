//
//  StoryDetailsViewController.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import UIKit
import SafariServices

class StoryDetailsViewController: UIViewController, NavBarDelegate {
    
    var navView: NavigationBarView!
    var detailTableView: UITableView!
    var imageView: UIImageView!
    var aboutLabel: UILabel!
    var userLabel: UILabel!
    var followersCount: UILabel!
    var followingCount: UILabel!
    var profileButton: UIButton!
    var readStoryButton: UIButton!
    
    var scrollView: UIScrollView!
    var bgView: UIView!
    
    var safariView:UIView?
    let containerView = UIView()
    
    var user: User?
    var story: Story? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        refreshData()

    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
   
    // MARK: - Button Actions
    
    func profileButtonTapped() {
        let svc = SFSafariViewController(URL: NSURL(string: user!.url)!)
        self.presentViewController(svc, animated: true, completion: nil)
    }
    
    func readStoryButtonTapped() {
        let svc = SFSafariViewController(URL: NSURL(string: story!.storyURL)!)
        self.presentViewController(svc, animated: true, completion: nil)
        
    }
    
     // MARK : - Custom Methods
    
    func refreshData() {
        if let imageURL = user?.imageURL {
            let url = NSURL(string: imageURL)
            
            imageView.sd_setImageWithURL(url, completed: { (image, error, cache, url) in
                
            })
        }
        
        userLabel.text = user?.username
        aboutLabel.text = user?.about
        followersCount.text = "Followers: \(user!.followersCount)"
        followingCount.text = "Following: \(user!.followingCount)"
    }
    
    // MARK : - Delegate Methods
    
    func backButtonTapped() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension StoryDetailsViewController {
    
    // MARK: -UIView Setup
    
    func configureSubviews() {
        
        self.view.backgroundColor = UIColor(white: 0.8, alpha: 1)
        
        
        // Navigation Bar View
        
        navView = NavigationBarView()
        navView.delegate = self
        navView.titleText = "Story Details"
        navView.backgroundColor = UIColor(red: 21/255, green: 107/255, blue: 255/255, alpha: 1)
        navView.backButton.hidden = false
        navView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = navView.topAnchor.constraintEqualToAnchor(self.view.topAnchor)
        let leadingConstraint = navView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
        let trailingConstraint = navView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
        let height = navView.heightAnchor.constraintEqualToConstant(64)
        
        self.view.addSubview(navView)
        
        NSLayoutConstraint.activateConstraints([topConstraint,leadingConstraint,trailingConstraint,height])
        
        bgView = UIView()
        bgView.layer.cornerRadius = 4
        bgView.layer.shadowOffset = CGSize(width: 2, height: 2)
        bgView.layer.shadowRadius = 2
        bgView.layer.shadowColor = UIColor(white: 0.5, alpha: 1).CGColor
        bgView.layer.shadowOpacity = 1
        self.view.clipsToBounds = true
        self.view.layer.masksToBounds = true

        bgView.backgroundColor = UIColor.whiteColor()
        bgView.translatesAutoresizingMaskIntoConstraints = false
        
        let bgViewTopConstraint = bgView.topAnchor.constraintEqualToAnchor(self.navView.bottomAnchor, constant: 8)
        let bgViewBottomConstraint = bgView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor, constant: -8)
        let bgViewLeadingConstraint = bgView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: 8)
        let bgViewTrailingConstraint = bgView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -8)
        self.view.addSubview(bgView)
        
        NSLayoutConstraint.activateConstraints([bgViewTopConstraint, bgViewBottomConstraint, bgViewLeadingConstraint, bgViewTrailingConstraint])
        
        imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.clearColor()
        imageView.contentMode = .ScaleAspectFit
        let bgImageViewTopConstraint = imageView.topAnchor.constraintEqualToAnchor(bgView.topAnchor, constant: 10)
        let bgImageViewLeadingConstraint = imageView.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor)
        let bgImageViewTrailingConstraint = imageView.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor)
        let bgImageViewHeightConstraint = imageView.heightAnchor.constraintEqualToConstant(150)
        
        bgView.addSubview(imageView)
        NSLayoutConstraint.activateConstraints([bgImageViewTopConstraint, bgImageViewLeadingConstraint, bgImageViewTrailingConstraint, bgImageViewHeightConstraint])
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor.lightGrayColor()
        seperatorView.translatesAutoresizingMaskIntoConstraints = false
        
        let seperatorViewTopConstraint = seperatorView.topAnchor.constraintEqualToAnchor(imageView.bottomAnchor, constant: 10)
        let seperatorViewLeadingConstraint = seperatorView.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor, constant: 20)
        let seperatorViewTrailingConstraint = seperatorView.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor, constant: -20)
        let seperatorViewHeightConstraint = seperatorView.heightAnchor.constraintEqualToConstant(1)
        
        bgView.addSubview(seperatorView)
        NSLayoutConstraint.activateConstraints([seperatorViewTopConstraint,seperatorViewLeadingConstraint,seperatorViewTrailingConstraint,seperatorViewHeightConstraint])
        
        userLabel = UILabel()
        userLabel.numberOfLines = 0
        userLabel.font = UIFont(name: "Helvetica", size: 18)
        userLabel.textAlignment = .Center
        userLabel.text = "userLabel"
        userLabel.textColor = UIColor(white: 0.2, alpha: 1)
        userLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let userLabelTopConstraint = userLabel.topAnchor.constraintEqualToAnchor(seperatorView.bottomAnchor, constant: 10)
        let userLabelleadingConstraint = userLabel.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor,constant: 5)
        let userLabelTrailingConstraint = userLabel.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor, constant: -5)
        
        bgView.addSubview(userLabel)
        NSLayoutConstraint.activateConstraints([userLabelTopConstraint, userLabelleadingConstraint, userLabelTrailingConstraint])
        
        aboutLabel = UILabel()
        aboutLabel.numberOfLines = 0
        aboutLabel.font = UIFont(name: "Helvetica", size: 14)
        aboutLabel.textAlignment = .Center
        aboutLabel.textColor = UIColor(white: 0.35, alpha: 1)

        aboutLabel.text = "aboutLabel"
        aboutLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let aboutLabelTopConstraint = aboutLabel.topAnchor.constraintEqualToAnchor(userLabel.bottomAnchor, constant: 10)
        let aboutLabelleadingConstraint = aboutLabel.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor,constant: 5)
        let aboutLabelTrailingConstraint = aboutLabel.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor, constant: -5)
        
        bgView.addSubview(aboutLabel)
        NSLayoutConstraint.activateConstraints([aboutLabelTopConstraint, aboutLabelleadingConstraint, aboutLabelTrailingConstraint])
        
        followersCount = UILabel()
        followingCount = UILabel()
        
        followersCount.numberOfLines = 0
        followersCount.font = UIFont(name: "Helvetica", size: 14)
        followersCount.textAlignment = .Center
        followersCount.text = "followersCount"
        followersCount.textColor = UIColor(white: 0.35, alpha: 1)

        followersCount.translatesAutoresizingMaskIntoConstraints = false
        
        let followersCountTopConstraint = followersCount.topAnchor.constraintEqualToAnchor(aboutLabel.bottomAnchor, constant: 10)
        let followersCountleadingConstraint = followersCount.leadingAnchor.constraintEqualToAnchor(bgView.leadingAnchor,constant: 5)
        let followersCountWidthConstraint = followersCount.widthAnchor.constraintEqualToConstant(screenWidth/2)
        
        
        bgView.addSubview(followersCount)
        
        followingCount.numberOfLines = 0
        followingCount.font = UIFont(name: "Helvetica", size: 14)
        followingCount.textAlignment = .Center
        followingCount.text = "followingCount"
        followingCount.textColor = UIColor(white: 0.35, alpha: 1)

        followingCount.translatesAutoresizingMaskIntoConstraints = false
        
        let followingCountTopConstraint = followingCount.topAnchor.constraintEqualToAnchor(aboutLabel.bottomAnchor, constant: 10)
        let followingCountleadingConstraint = followingCount.trailingAnchor.constraintEqualToAnchor(bgView.trailingAnchor,constant: -5)
        let followingCountWidthConstraint = followingCount.widthAnchor.constraintEqualToConstant(screenWidth/2)
        
        bgView.addSubview(followingCount)
        
        NSLayoutConstraint.activateConstraints([followersCountTopConstraint, followersCountleadingConstraint, followersCountWidthConstraint])
        NSLayoutConstraint.activateConstraints([followingCountTopConstraint, followingCountleadingConstraint, followingCountWidthConstraint])
        
        profileButton = UIButton()
        profileButton.setTitle("Check Profile", forState: UIControlState.Normal)
        profileButton.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
        profileButton.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        profileButton.translatesAutoresizingMaskIntoConstraints = false
        profileButton.addTarget(self, action: #selector(StoryDetailsViewController.profileButtonTapped), forControlEvents: .TouchUpInside)
        
        let profileButtonTopConstraint = profileButton.topAnchor.constraintEqualToAnchor(self.followingCount.bottomAnchor, constant: 15)
        let profileButtonXConstraint = profileButton.centerXAnchor.constraintEqualToAnchor(self.bgView.centerXAnchor)
        let profileButtonHeight = profileButton.heightAnchor.constraintEqualToConstant(40)
        let profileButtonWidth = profileButton.widthAnchor.constraintEqualToConstant(150)
        bgView.addSubview(profileButton)
        
        NSLayoutConstraint.activateConstraints([profileButtonTopConstraint,profileButtonXConstraint, profileButtonHeight, profileButtonWidth])
        
        readStoryButton = UIButton()
        readStoryButton.setTitle("Read Full Story", forState: UIControlState.Normal)
        readStoryButton.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
        readStoryButton.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        readStoryButton.translatesAutoresizingMaskIntoConstraints = false
        readStoryButton.addTarget(self, action: #selector(StoryDetailsViewController.readStoryButtonTapped), forControlEvents: .TouchUpInside)
        
        let readStoryButtonTopConstraint = readStoryButton.topAnchor.constraintEqualToAnchor(self.profileButton.bottomAnchor)
        let readStoryButtonXConstraint = readStoryButton.centerXAnchor.constraintEqualToAnchor(self.bgView.centerXAnchor)
        let readStoryButtonHeight = readStoryButton.heightAnchor.constraintEqualToConstant(40)
        let readStoryButtonWidth = readStoryButton.widthAnchor.constraintEqualToConstant(150)
        
        bgView.addSubview(readStoryButton)
        
        NSLayoutConstraint.activateConstraints([readStoryButtonTopConstraint,readStoryButtonXConstraint, readStoryButtonHeight, readStoryButtonWidth])
        
    }

}


