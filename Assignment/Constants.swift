//
//  Constants.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import Foundation
import UIKit

let screenHeight = UIScreen.mainScreen().bounds.size.height
let screenWidth = UIScreen.mainScreen().bounds.size.width

let url = NSBundle.mainBundle().URLForResource("Data", withExtension: "json")
let data = NSData(contentsOfURL: url!)