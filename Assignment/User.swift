//
//  User.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import Foundation

struct User {
    
    let about : String!
    let userID : String!
    let username : String!
    let followersCount : Int!
    let followingCount: Int!
    let imageURL: String!
    let url: String!
    let handle: String!
    let isFollowing: Bool!
    let createdOn: Int!
    
}