//
//  JsonManager.swift
//  Assignment
//
//  Created by Karan Kumar on 07/05/16.
//  Copyright © 2016 Karan Kumar. All rights reserved.
//

import Foundation

class JsonManager {
    
    static let sharedInstance = JsonManager()
    
    var storyArray = [Story]()
    var userArray = [User]()
    
    func parseJsonData() -> ([Story], [User]) {
        do {
            let object = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            if let dataArray = object as? [[String: AnyObject]] {
                for (index, data) in dataArray.enumerate() {
                    if index > 1 {
                        let storyID = data["id"] as! String
                        let title = data["title"] as! String
                        let type = data["type"] as! String
                        let titleDescription = data["description"] as! String
                        let imageURL = data["si"] as! String
                        let storyURL = data["url"] as! String
                        let isLiked = data["like_flag"] as! Bool
                        let commentsCount = data["comment_count"] as! Int
                        let likesCount = data["likes_count"] as! Int
                        let db = data["db"] as! String
                        storyArray.append(Story(storyID: storyID, db: db, title: title, type: type, titleDescription: titleDescription, bgImageURL: imageURL, storyURL: storyURL, isLiked: isLiked, likesCount: likesCount, commentCount: commentsCount))
                        
                    } else {
                        
                        let about = data["about"] as! String
                        let userID =  data["id"] as! String
                        let username = data["username"] as!  String
                        let followersCount = data["followers"] as!  Int
                        let followingCount = data["following"] as!  Int
                        let imageURL = data["image"] as!  String
                        let url = data["url"] as!  String
                        let handle = data["handle"] as!  String
                        let isFollowing = data["is_following"] as!  Bool
                        let createdOn = data["createdOn"] as!  Int
                        
                        userArray.append(User(about: about, userID: userID, username: username, followersCount: followersCount, followingCount: followingCount, imageURL: imageURL, url: url, handle: handle, isFollowing: isFollowing, createdOn: createdOn))
                        
                    }
                }
                
                return (storyArray, userArray)
            }
        } catch {
            // Handle Error
        }
        return ([Story](), [User]())
    }

}